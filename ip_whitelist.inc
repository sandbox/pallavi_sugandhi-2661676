<?php

/**
 * @file
 * provides the setting form for menu item.
 */

/**
 * Callback function for menu.
 */
function ip_whitelist_form($form, &$form_state) {
  $form['ip_address'] = array(
    '#type' => 'textfield',
    '#title' => t('IP Address Range'),
    '#default_value' => variable_get('ip_address'),
    '#size' => 62,
    '#description' => t('Provide the range of IP address to which you want to allow the access of the below mentioned URL e.g. 1.2.3.4-1.2.3.64, 1.2.3.200-1.2.3.200 etc'),
    '#required' => TRUE,
  );
   $form['allowed_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Node URL'),
    '#default_value' => variable_get('allowed_url'),
    '#size' => 62,
    '#description' => t('Allowed URL to the above mentioned IP range such as : story or story/*'),
    '#required' => TRUE,
  );
   $form['redirect_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URL'),
    '#default_value' => variable_get('redirect_url'),
    '#size' => 62,
    '#description' => t('Redirect URL for non-whitelisted IP range such as : <front> or /access-denied or node/132'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
