MODULE
------
IP Whitelist

DESCRIPTION/FEATURES
--------------------
   IP Whitelist module allows the user to provide access on particular node to particular set of IP address(s).
   IP Whitelist Module provides an interface to the admin user to provide the range IP address to allow them access to a specific node. 
   The module allows the administrator to specify a redirect URL as well incase the specific page is accessed by a non-whitelisted IP.

REQUIREMENTS
------------
Drupal 7.0
	
INSTALLATION
------------
1. Download the IP Whitelist
2. Uncompress the module and place it in the modules directory ('sites/all/modules')
3. Go to the '/admin/modules' page and enable the module

CONFIGURATION
-------------
The settings for the module can be configured at the admin configuration page. 
Navigate to the 'admin/config/people/ip_config' page.
The page allows us to set the below mentioned parameters
i) IP Address Range: The address range to which the specific node is accessible
ii) Node URL: The specific node URL allowed for the IP range specified in the Step (i)
iii) Redirect URL: The URL to which the request from non-whitelisted IP address has to be redirected.
